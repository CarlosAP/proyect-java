/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobro_matricula_uestudiantes;

/**
 *
 * @author Lenovo
 */
public class Descuentos extends Pagos {
    
     protected  double descuentoA, descuentoB, descuentoC, descuentoAplicado, pagoMatricula;

     ///Constructor
     public Descuentos(int pagoTotal, int totalRecaudado, int totalEstudiantesAlimentacion,
             double descuentoA, double descuentoB, double descuentoC, double descuentoAplicado, double pagoMatricula){
         super(pagoTotal, totalRecaudado, totalEstudiantesAlimentacion);
         this.descuentoA = descuentoA;
         this.descuentoB = descuentoB;
         this.descuentoC = descuentoC;
         this.descuentoAplicado = descuentoAplicado;
         this.pagoMatricula = pagoMatricula;
     }
     
     
     ///Get & Set
    public double getDescuentoA() {
        return descuentoA;
    }

    public void setDescuentoA(double descuentoA) {
        this.descuentoA = descuentoA;
    }

    public double getDescuentoB() {
        return descuentoB;
    }

    public void setDescuentoB(double descuentoB) {
        this.descuentoB = descuentoB;
    }

    public double getDescuentoC() {
        return descuentoC;
    }

    public void setDescuentoC(double descuentoC) {
        this.descuentoC = descuentoC;
    }

    public double getDescuentoAplicado() {
        return descuentoAplicado;
    }

    public void setDescuentoAplicado(double descuentoAplicado) {
        this.descuentoAplicado = descuentoAplicado;
    }

    public double getPagoMatricula() {
        return pagoMatricula;
    }

    public void setPagoMatricula(double pagoMatricula) {
        this.pagoMatricula = pagoMatricula;
    }
    
     
     
}
