/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobro_matricula_uestudiantes;

/**
 *
 * @author Lenovo
 */
public class Pagos {
    
    protected int pagoTotal,totalRecaudado, totalEstudiantesAlimentacion;
    
    
    ///Constructor
    public Pagos(int pagoTotal, int totalRecaudado, int totalEstudiantesAlimentacion) {    
        this.pagoTotal = pagoTotal;
        this.totalRecaudado = totalRecaudado;
        this.totalEstudiantesAlimentacion = totalEstudiantesAlimentacion;
    }

    ///Getters & Set
    public int getPagoTotal() {
        return pagoTotal;
    }

    public void setPagoTotal(int pagoTotal) {
        this.pagoTotal = pagoTotal;
    }

    public int getTotalRecaudado() {
        return totalRecaudado;
    }

    public void setTotalRecaudado(int totalRecaudado) {
        this.totalRecaudado = totalRecaudado;
    }

    public int getTotalEstudiantesAlimentacion() {
        return totalEstudiantesAlimentacion;
    }

    public void setTotalEstudiantesAlimentacion(int totalEstudiantesAlimentacion) {
        this.totalEstudiantesAlimentacion = totalEstudiantesAlimentacion;
    }

  
    
}
